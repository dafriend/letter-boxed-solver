Solver for the "Letter Boxed" puzzle from the New York Times: https://www.nytimes.com/puzzles/letter-boxed

"scrabble_words.txt" is used for the list of words. This does differ slightly from the list used by the puzzle itself - I've come across words in "scrabble_words.txt" that aren't accepted by the puzzle. But I wasn't able to find the actual word list the puzzle uses.

In "letter_boxed_solver.R" the only variable that needs to be changed is `ltrs_mat`.
